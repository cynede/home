Dot files
=========

<img align="right" src="http://files.gamebanana.com/img/ico/sprays/50decef29d1ed.gif">

 - .xmonad - https://github.com/Heather/xmonad
 - .zsh - https://github.com/Heather/zsh
 - .emacs.d - https://github.com/Heather/clarity
 - .nensha/* - random stuff...
