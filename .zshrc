. ~/.zsh/config.sh
. ~/.zsh/aliases.sh
. ~/.zsh/functions.sh
. ~/.zsh/keys.sh
. ~/.zsh/ssh.sh

[[ $OSTYPE == darwin*  && -f ~/.zsh/darwin.sh ]]  && source ~/.zsh/darwin.sh
[[ $OSTYPE == linux*   && -f ~/.zsh/linux.sh ]]   && source ~/.zsh/linux.sh
[[ $OSTYPE == solaris* && -f ~/.zsh/solaris.sh ]] && source ~/.zsh/solaris.sh
[[ $OSTYPE == freebsd* && -f ~/.zsh/freebsd.sh ]] && source ~/.zsh/freebsd.sh
